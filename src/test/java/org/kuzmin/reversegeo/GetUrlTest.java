package org.kuzmin.reversegeo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.liferay.portal.kernel.util.StringUtil;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/services-test-config.xml"})
public class GetUrlTest extends AbstractTest {

    /**
     * Тестируем нормальное поведение
     */
    @Test
    public void test01() throws IOException {
        test("case01");
    }

    /**
     * Отсутствует долгота, вместо url должен быть null
     */
    @Test
    public void test02() throws IOException {
        test("case02");
    }

    /**
     * Отсутствует широта, вместо url должен быть null
     */
    @Test
    public void test03() throws IOException {
        test("case03");
    }

    /**
     * Отсутствует локаль, вместо url должен быть null
     */
    @Test
    public void test04() throws IOException {
        test("case04");
    }

    /**
     * Английская локаль
     */
    @Test
    public void test05() throws IOException {
        test("case05");
    }

    /**
     * Белорусская локаль
     */
    @Test
    public void test06() throws IOException {
        test("case06");
    }

    /**
     * Украинская локаль
     */
    @Test
    public void test07() throws IOException {
        test("case07");
    }

    /**
     * Неправильная локаль
     */
    @Test
    public void test08() throws IOException {
        test("case08");
    }

    /**
     * Отрицательные координаты
     */
    @Test
    public void test09() throws IOException {
        test("case09");
    }

    private void test(String testCase) throws IOException {
        File input = getFile(testCase, "input.txt");
        FileInputStream inputStream = new FileInputStream(input);
        BufferedReader inputReader = new BufferedReader(new InputStreamReader(inputStream));

        File expected = getFile(testCase, "expected.txt");
        FileInputStream expectedStream = new FileInputStream(expected);
        BufferedReader expectedReader = new BufferedReader(new InputStreamReader(expectedStream));

        Double longitude = null;
        Double latitude = null;
        String locale = null;

        String expectedUrl = null;

        try {
            String longitudeString = inputReader.readLine();
            if (longitudeString != null && !longitudeString.isEmpty()) {
                longitude = Double.valueOf(longitudeString);
            }

            String latitudeString = inputReader.readLine();
            if (latitudeString != null && !latitudeString.isEmpty()) {
                latitude = Double.valueOf(latitudeString);
            }

            locale = inputReader.readLine();

            expectedUrl = expectedReader.readLine();
        } catch (IOException e) {
            //It's ok in case we would want to test for null values
        } finally {
            inputReader.close();
            expectedReader.close();
        }

        String actualUrl = reverseGeoService.getUrl(longitude, latitude, locale);
        Assert.assertTrue(String.format("Actual url and expected url are different.\nActual url: %s\nExpected url: %s", actualUrl, expectedUrl), 
                StringUtil.equalsIgnoreCase(actualUrl, expectedUrl));
    }
}
