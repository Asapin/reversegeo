package org.kuzmin.reversegeo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.kuzmin.reversegeo.converter.JSONConverter;
import org.kuzmin.reversegeo.models.GeoObjectModel;

public class JSONConverterTest extends AbstractTest {

    @Test
    public void test01() throws IOException {
        test("case01");
    }

    @Test
    public void test02() throws IOException {
        test("case02");
    }

    private void test(String testCase) throws IOException {
        List<GeoObjectModel> actualResult = getActualResult(testCase);
        List<GeoObjectModel> expectedResult = getExpectedResult(testCase);
        Assert.assertTrue("There are some problems with converting JSON to objects", expectedResult.equals(actualResult));
    }

    private List<GeoObjectModel> getActualResult(String testCase) throws IOException {
        File input = getFile(testCase, "input.txt");
        FileInputStream inputStream = new FileInputStream(input);
        BufferedReader inputReader = new BufferedReader(new InputStreamReader(inputStream));

        List<GeoObjectModel> actualResult = new ArrayList<GeoObjectModel>();
        try {
            String json = inputReader.readLine();
            JSONConverter converter = new JSONConverter();
            actualResult.addAll(converter.convert(json));
        } finally {
            inputReader.close();
        }
        return actualResult;
    }

    private List<GeoObjectModel> getExpectedResult(String testCase) throws IOException {
        File expected = getFile(testCase, "expected.txt");
        FileInputStream expectedStream = new FileInputStream(expected);
        BufferedReader expectedReader = new BufferedReader(new InputStreamReader(expectedStream));

        List<GeoObjectModel> result = new ArrayList<GeoObjectModel>();
        String line;
        try {
            while ((line = expectedReader.readLine()) != null) {
                String[] object = line.split("\t");
                GeoObjectModel geoObjectModel = new GeoObjectModel();
                int i = 0;
                geoObjectModel.setKind(object[i++]);
                geoObjectModel.setCountryName(object[i++]);
                geoObjectModel.setAdministrativeAreaName(object[i++]);
                geoObjectModel.setSubAdministrativeAreaName(object[i++]);
                geoObjectModel.setLocalityName(object[i++]);
                geoObjectModel.setThoroughfareName(object[i++]);
                geoObjectModel.setPremiseNumber(object[i++]);

                result.add(geoObjectModel);
            }
        } finally {
            expectedReader.close();
        }
        return result;
    }
}
