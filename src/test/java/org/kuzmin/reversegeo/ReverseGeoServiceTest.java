package org.kuzmin.reversegeo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.kuzmin.reversegeo.models.Coordinates;
import org.kuzmin.reversegeo.models.GeoObjectModel;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/services-test-config.xml"})
public class ReverseGeoServiceTest extends AbstractTest {

    @Test
    public void test() throws Exception {
        Coordinates coordinates = new Coordinates();
        coordinates.setLongitude(1d);
        coordinates.setLatitude(1d);
        coordinates.setLocale("ru_RU");
        List<GeoObjectModel> actualResult = reverseGeoService.retrieveGeoObjects(coordinates);
        List<GeoObjectModel> expectedResult = getExpectedResult("case01");
        Assert.assertTrue("There are some problems with reverse geo service", expectedResult.equals(actualResult));
    }

    private List<GeoObjectModel> getExpectedResult(String testCase) throws IOException {
        File expected = getFile(testCase, "expected.txt");
        FileInputStream expectedStream = new FileInputStream(expected);
        BufferedReader expectedReader = new BufferedReader(new InputStreamReader(expectedStream));

        List<GeoObjectModel> result = new ArrayList<GeoObjectModel>();
        String line;
        try {
            while ((line = expectedReader.readLine()) != null) {
                String[] object = line.split("\t");
                GeoObjectModel geoObjectModel = new GeoObjectModel();
                int i = 0;
                geoObjectModel.setKind(object[i++]);
                geoObjectModel.setCountryName(object[i++]);
                geoObjectModel.setAdministrativeAreaName(object[i++]);
                geoObjectModel.setSubAdministrativeAreaName(object[i++]);
                geoObjectModel.setLocalityName(object[i++]);
                geoObjectModel.setThoroughfareName(object[i++]);
                geoObjectModel.setPremiseNumber(object[i++]);

                result.add(geoObjectModel);
            }
        } finally {
            expectedReader.close();
        }
        return result;
    }
}
