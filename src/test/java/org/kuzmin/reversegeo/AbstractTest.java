package org.kuzmin.reversegeo;

import java.io.File;
import java.net.URL;

import org.apache.commons.lang3.StringUtils;
import org.kuzmin.reversegeo.interfaces.ReverseGeoService;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractTest {

    @Autowired
    protected ReverseGeoService reverseGeoService;

    protected File getFile(String testCase, String fileName) {
        URL location = this.getClass().getProtectionDomain().getCodeSource().getLocation();
        String filePath = StringUtils.join(new String[] {location.getPath(), this.getClass().getSimpleName(), testCase, fileName}, File.separator);
        File file = new File(filePath);
        return file;
    }

}
