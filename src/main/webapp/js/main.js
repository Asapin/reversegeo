function isInputEmpty() {
	var longitude = $("#longitude").val();
	var latitude = $("#latitude").val();
	if (longitude == "" || latitude == "") {
		return false;
	}
	return true;
}

function sanitize(inputElement) {
	var caretPosition = $(inputElement).caret().start;
	var sanitized = $(inputElement).val().replace(/[^-.0-9]/g, '');
	sanitized = sanitized.replace(/(.)-+/g, '$1');
	sanitized = sanitized.replace(/\.(?=.*\.)/g, '');
	$(inputElement).val(sanitized);
	if (caretPosition > sanitized.length) {
		caretPosition = sanitized.length - 1;
	}
	$(inputElement).caret(caretPosition, caretPosition);
}