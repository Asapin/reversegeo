<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<portlet:actionURL var="submitFormURL">
	<portlet:param name="action" value="handleRecoding"></portlet:param>
</portlet:actionURL>

<link href="<%=request.getContextPath()%>/css/main.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-1.11.3.min.js"></script>
<script language="JavaScript" type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.browser.min.js"></script>
<script language="JavaScript" type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.caret.1.02.min.js"></script>
<script language="JavaScript" type="text/javascript" src="<%=request.getContextPath()%>/js/main.js"></script>

<form:form name="coordinates" modelAttribute="coordinates" method="post" action="${submitFormURL}" >
	<table>
		<tr>
			<td>
				<form:label path="longitude">Долгота</form:label>
				<form:input path="longitude" id="longitude" cssClass="user-input" onkeyup="sanitize(this);" placeholder="Введите долготу"></form:input>
			</td>
			<td>
				<form:label path="latitude">Широта</form:label>
				<form:input path="latitude" id="latitude" cssClass="user-input" onkeyup="sanitize(this);" placeholder="Введите широту"></form:input>
			</td>
			<td>
				<form:label path="locale">Язык</form:label>
				<form:select path="locale" cssClass="user-input">
					<c:forEach var="locale" items="${locales}">
						<form:option value="${locale.key}">${locale.value}</form:option>
				    </c:forEach>
				</form:select>
			</td>
		</tr>
	</table>
	<input type="submit" value="Преобразовать" onClick="return isInputEmpty();"/>
</form:form>

<c:if test="${fn:length(geoObjects) gt 0}">
     <table class="geo-table">
		<thead>
			<tr>
				<th>Страна</th>
				<th>Регион</th>
				<th>Округ</th>
				<th>Населенный пункт</th>
				<th>Улица</th>
				<th>Дом</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="geoObject" items="${geoObjects}" varStatus="loopStatus">
				<tr class="${loopStatus.index % 2 == 0 ? '' : 'grey'}">
					<th>${geoObject.countryName}</th>
					<th>${geoObject.administrativeAreaName}</th>
					<th>${geoObject.subAdministrativeAreaName}</th>
					<th>${geoObject.localityName}</th>
					<th>${geoObject.thoroughfareName}</th>
					<th>${geoObject.premiseNumber}</th>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</c:if>