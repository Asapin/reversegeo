package org.kuzmin.reversegeo.exceptions;

public class IncorrectParametersException extends Exception {

    private static final long serialVersionUID = -3371431401936548873L;

    public IncorrectParametersException() {
        super();
    }

    public IncorrectParametersException(String message, Throwable cause,
            boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public IncorrectParametersException(String message, Throwable cause) {
        super(message, cause);
    }

    public IncorrectParametersException(String message) {
        super(message);
    }

    public IncorrectParametersException(Throwable cause) {
        super(cause);
    }

}
