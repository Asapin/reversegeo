package org.kuzmin.reversegeo.controller;

import java.util.List;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.apache.log4j.Logger;
import org.kuzmin.reversegeo.interfaces.LocaleService;
import org.kuzmin.reversegeo.interfaces.ReverseGeoService;
import org.kuzmin.reversegeo.models.Coordinates;
import org.kuzmin.reversegeo.models.GeoObjectModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

@Controller("mainController")
@RequestMapping(value = "VIEW")
public class MainController {

    final static protected Logger log = Logger.getLogger(MainController.class);

    @Autowired
    private LocaleService localeService;
    @Autowired
    private ReverseGeoService reverseGeoService;

    @RenderMapping
    protected String showMainPage(Model model) {
        fillModel(model);
        return "mainView";
    }

    @ActionMapping(params = "action=handleRecoding")
    public void getReverseGeocode(ActionRequest actionRequest,
            ActionResponse actionResponse, Model model,
            @ModelAttribute("coordinates") Coordinates coordinates) {
        fillModel(model);
        List<GeoObjectModel> geoObjects = null;
        try {
            geoObjects = reverseGeoService.retrieveGeoObjects(coordinates);
            model.addAttribute("geoObjects", geoObjects);
            actionResponse.setRenderParameter("action", "recoded");
        } catch (Exception e) {
            log.error("Could not retrieve or convert objects from Yandex Geocoding Service", e);
            model.addAttribute("msg", e.getMessage());
            actionResponse.setRenderParameter("action", "error");
        }
    }

    @RenderMapping(params = "action=recoded")
    public String viewRecoded(Model model) {
        fillModel(model);
        return "mainView";
    }

    @RenderMapping(params = "action=error")
    public String viewError() {
        return "error";
    }

    private void fillModel(Model model) {
        if (!model.asMap().containsKey("locales")) {
            Map locales = localeService.getAvailableLocales();
            model.addAttribute("locales", locales);
        }
        if (!model.asMap().containsKey("coordinates")) {
            Coordinates coordinates = new Coordinates();
            model.addAttribute("coordinates", coordinates);
        }
    }
}
