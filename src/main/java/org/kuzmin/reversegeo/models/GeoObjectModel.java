package org.kuzmin.reversegeo.models;

import java.io.Serializable;

public class GeoObjectModel implements Serializable {
    private static final long serialVersionUID = 7336001601671920595L;

    private String kind;
    private String countryName;
    private String administrativeAreaName;
    private String subAdministrativeAreaName;
    private String localityName;
    private String thoroughfareName;
    private String premiseNumber;

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getAdministrativeAreaName() {
        return administrativeAreaName;
    }

    public void setAdministrativeAreaName(String administrativeAreaName) {
        this.administrativeAreaName = administrativeAreaName;
    }

    public String getSubAdministrativeAreaName() {
        return subAdministrativeAreaName;
    }

    public void setSubAdministrativeAreaName(String subAdministrativeAreaName) {
        this.subAdministrativeAreaName = subAdministrativeAreaName;
    }

    public String getLocalityName() {
        return localityName;
    }

    public void setLocalityName(String localityName) {
        this.localityName = localityName;
    }

    public String getThoroughfareName() {
        return thoroughfareName;
    }

    public void setThoroughfareName(String thoroughfareName) {
        this.thoroughfareName = thoroughfareName;
    }

    public String getPremiseNumber() {
        return premiseNumber;
    }

    public void setPremiseNumber(String premiseNumber) {
        this.premiseNumber = premiseNumber;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((administrativeAreaName == null) ? 0 : administrativeAreaName.hashCode());
        result = prime * result + ((countryName == null) ? 0 : countryName.hashCode());
        result = prime * result + ((kind == null) ? 0 : kind.hashCode());
        result = prime * result + ((localityName == null) ? 0 : localityName.hashCode());
        result = prime * result + ((premiseNumber == null) ? 0 : premiseNumber.hashCode());
        result = prime * result + ((subAdministrativeAreaName == null) ? 0 : subAdministrativeAreaName.hashCode());
        result = prime * result + ((thoroughfareName == null) ? 0 : thoroughfareName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        GeoObjectModel other = (GeoObjectModel) obj;
        if (administrativeAreaName == null) {
            if (other.administrativeAreaName != null)
                return false;
        } else if (!administrativeAreaName.equals(other.administrativeAreaName))
            return false;
        if (countryName == null) {
            if (other.countryName != null)
                return false;
        } else if (!countryName.equals(other.countryName))
            return false;
        if (kind == null) {
            if (other.kind != null)
                return false;
        } else if (!kind.equals(other.kind))
            return false;
        if (localityName == null) {
            if (other.localityName != null)
                return false;
        } else if (!localityName.equals(other.localityName))
            return false;
        if (premiseNumber == null) {
            if (other.premiseNumber != null)
                return false;
        } else if (!premiseNumber.equals(other.premiseNumber))
            return false;
        if (subAdministrativeAreaName == null) {
            if (other.subAdministrativeAreaName != null)
                return false;
        } else if (!subAdministrativeAreaName
                .equals(other.subAdministrativeAreaName))
            return false;
        if (thoroughfareName == null) {
            if (other.thoroughfareName != null)
                return false;
        } else if (!thoroughfareName.equals(other.thoroughfareName))
            return false;
        return true;
    }
}
