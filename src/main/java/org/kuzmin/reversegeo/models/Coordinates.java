package org.kuzmin.reversegeo.models;

import java.io.Serializable;

public class Coordinates implements Serializable {
    private static final long serialVersionUID = -3220726525068226417L;
    private Double longitude;
    private Double latitude;
    private String locale;

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }
}
