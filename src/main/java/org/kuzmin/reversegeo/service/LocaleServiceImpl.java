package org.kuzmin.reversegeo.service;

import java.util.HashMap;
import java.util.Map;

import org.kuzmin.reversegeo.interfaces.LocaleService;
import org.springframework.stereotype.Service;

@Service(value = "localeService")
public class LocaleServiceImpl implements LocaleService {

    private static final Map<String, String> locales = new HashMap<String, String>();
    static {
        initLocales();
    }

    @Override
    public Map<String, String> getAvailableLocales() {
        return locales;
    }

    private static void initLocales() {
        locales.put("ru_RU", "Русский");
        locales.put("uk_UA", "Украинский");
        locales.put("be_BY", "Белорусский");
        locales.put("en_US", "Английский");
    }

    @Override
    public boolean checkLocaleCorrect(String locale) {
        return locales.containsKey(locale);
    }

}
