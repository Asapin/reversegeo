package org.kuzmin.reversegeo.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Locale;

import org.kuzmin.reversegeo.converter.JSONConverter;
import org.kuzmin.reversegeo.exceptions.IncorrectParametersException;
import org.kuzmin.reversegeo.interfaces.LocaleService;
import org.kuzmin.reversegeo.interfaces.ReverseGeoService;
import org.kuzmin.reversegeo.models.Coordinates;
import org.kuzmin.reversegeo.models.GeoObjectModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service(value = "reverseGeoService")
public class ReverseGeoServiceImpl implements ReverseGeoService {

    private static final String GEOCODE_URL_TEMPLATE = "https://geocode-maps.yandex.ru/1.x/?geocode=%.4f,%.4f&lang=%s&format=json&kind=house";

    @Autowired
    private LocaleService localeService;

    @Override
    public String getUrl(Double longitude, Double latitude, String locale) {
        if (longitude != null && latitude != null && locale != null) {
            if (localeService.checkLocaleCorrect(locale)) {
                return String.format(Locale.US, GEOCODE_URL_TEMPLATE, longitude, latitude, locale);
            }
        }
        return null;
    }

    @Override
    public List<GeoObjectModel> retrieveGeoObjects(Coordinates coordinates)
            throws Exception {
        String url = getUrl(coordinates.getLongitude(), coordinates.getLatitude(), coordinates.getLocale());
        if (url == null) {
            throw new IncorrectParametersException("Some parameters is null or locale is incorrect");
        }
        String jsonResponse = httpGet(url);
        JSONConverter converter = new JSONConverter();
        List<GeoObjectModel> result = converter.convert(jsonResponse);
        return result;
    }

    protected String httpGet(String urlStr) throws IOException {
        URL url = new URL(urlStr);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        if (conn.getResponseCode() != 200) {
            throw new IOException(conn.getResponseMessage());
        }

        StringBuilder sb = new StringBuilder();

        // Buffer the result into a string
        try (BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()))) {
            String line;
            while ((line = rd.readLine()) != null) {
                sb.append(line);
            }
        }

        conn.disconnect();
        return sb.toString();
    }
}
