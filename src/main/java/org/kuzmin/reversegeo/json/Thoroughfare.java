package org.kuzmin.reversegeo.json;

import com.google.gson.annotations.SerializedName;

public class Thoroughfare {
    @SerializedName("ThoroughfareName")
    private String thoroughfareName;
    @SerializedName("Premise")
    private Premise premise;

    public String getThoroughfareName() {
        return thoroughfareName;
    }

    public void setThoroughfareName(String thoroughfareName) {
        this.thoroughfareName = thoroughfareName;
    }

    public Premise getPremise() {
        return premise;
    }

    public void setPremise(Premise premise) {
        this.premise = premise;
    }
}
