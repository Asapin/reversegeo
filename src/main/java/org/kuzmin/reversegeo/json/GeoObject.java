package org.kuzmin.reversegeo.json;

import com.google.gson.annotations.SerializedName;

public class GeoObject {
    @SerializedName("metaDataProperty")
    private MetaDataProperty metaDataProperty;
    @SerializedName("description")
    private String description;
    @SerializedName("name")
    private String name;

    public MetaDataProperty getMetaDataProperty() {
        return metaDataProperty;
    }

    public void setMetaDataProperty(MetaDataProperty metaDataProperty) {
        this.metaDataProperty = metaDataProperty;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
