package org.kuzmin.reversegeo.json;

import com.google.gson.annotations.SerializedName;

public class SubAdministrativeArea {
    @SerializedName("SubAdministrativeAreaName")
    private String subAdministrativeAreaName;
    @SerializedName("Locality")
    private Locality locality;

    public String getSubAdministrativeAreaName() {
        return subAdministrativeAreaName;
    }

    public void setSubAdministrativeAreaName(String subAdministrativeAreaName) {
        this.subAdministrativeAreaName = subAdministrativeAreaName;
    }

    public Locality getLocality() {
        return locality;
    }

    public void setLocality(Locality locality) {
        this.locality = locality;
    }
}
