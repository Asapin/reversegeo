package org.kuzmin.reversegeo.json;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class GeoObjectCollection {
    @SerializedName("featureMember")
    private List<GeoObjectHolder> featureMember;

    public List<GeoObjectHolder> getFeatureMember() {
        return featureMember;
    }

    public void setFeatureMember(List<GeoObjectHolder> featureMember) {
        this.featureMember = featureMember;
    }

    public class GeoObjectHolder {
        @SerializedName("GeoObject")
        private GeoObject geoObject;

        public GeoObject getGeoObject() {
            return geoObject;
        }

        public void setGeoObject(GeoObject geoObject) {
            this.geoObject = geoObject;
        }
    }
}
