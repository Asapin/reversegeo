package org.kuzmin.reversegeo.json;

import com.google.gson.annotations.SerializedName;

public class GeocoderResponse {
    @SerializedName("response")
    private Response response;

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }
}
