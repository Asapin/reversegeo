package org.kuzmin.reversegeo.json;

import com.google.gson.annotations.SerializedName;

public class MetaDataProperty {
    @SerializedName("GeocoderMetaData")
    private GeocoderMetaData geocoderMetaData;

    public GeocoderMetaData getGeocoderMetaData() {
        return geocoderMetaData;
    }

    public void setGeocoderMetaData(GeocoderMetaData geocoderMetaData) {
        this.geocoderMetaData = geocoderMetaData;
    }
}
