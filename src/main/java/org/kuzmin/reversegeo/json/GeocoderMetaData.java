package org.kuzmin.reversegeo.json;

import com.google.gson.annotations.SerializedName;

public class GeocoderMetaData {
    @SerializedName("kind")
    private String kind;
    @SerializedName("text")
    private String text;
    @SerializedName("precision")
    private String precision;
    @SerializedName("AddressDetails")
    private AddressDetails addressDetails;

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getPrecision() {
        return precision;
    }

    public void setPrecision(String precision) {
        this.precision = precision;
    }

    public AddressDetails getAddressDetails() {
        return addressDetails;
    }

    public void setAddressDetails(AddressDetails addressDetails) {
        this.addressDetails = addressDetails;
    }
}
