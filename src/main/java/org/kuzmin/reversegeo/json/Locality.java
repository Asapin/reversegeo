package org.kuzmin.reversegeo.json;

import com.google.gson.annotations.SerializedName;

public class Locality {
    @SerializedName("LocalityName")
    private String localityName;
    @SerializedName("Thoroughfare")
    private Thoroughfare thoroughfare;

    public String getLocalityName() {
        return localityName;
    }

    public void setLocalityName(String localityName) {
        this.localityName = localityName;
    }

    public Thoroughfare getThoroughfare() {
        return thoroughfare;
    }

    public void setThoroughfare(Thoroughfare thoroughfare) {
        this.thoroughfare = thoroughfare;
    }
}
