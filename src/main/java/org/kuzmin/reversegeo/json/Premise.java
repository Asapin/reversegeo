package org.kuzmin.reversegeo.json;

import com.google.gson.annotations.SerializedName;

public class Premise {
    @SerializedName("PremiseNumber")
    private String premiseNumber;

    public String getPremiseNumber() {
        return premiseNumber;
    }

    public void setPremiseNumber(String premiseNumber) {
        this.premiseNumber = premiseNumber;
    }
}
