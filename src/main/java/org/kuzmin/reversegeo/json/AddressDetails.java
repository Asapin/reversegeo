package org.kuzmin.reversegeo.json;

import com.google.gson.annotations.SerializedName;

public class AddressDetails {
    @SerializedName("Country")
    private Country country;

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }
}
