package org.kuzmin.reversegeo.json;

import com.google.gson.annotations.SerializedName;

public class AdministrativeArea {
    @SerializedName("AdministrativeAreaName")
    private String administrativeAreaName;
    @SerializedName("SubAdministrativeArea")
    private SubAdministrativeArea subAdministrativeArea;

    public String getAdministrativeAreaName() {
        return administrativeAreaName;
    }

    public void setAdministrativeAreaName(String administrativeAreaName) {
        this.administrativeAreaName = administrativeAreaName;
    }

    public SubAdministrativeArea getSubAdministrativeArea() {
        return subAdministrativeArea;
    }

    public void setSubAdministrativeArea(SubAdministrativeArea subAdministrativeArea) {
        this.subAdministrativeArea = subAdministrativeArea;
    }
}
