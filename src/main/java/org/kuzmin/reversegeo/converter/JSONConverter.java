package org.kuzmin.reversegeo.converter;

import java.util.ArrayList;
import java.util.List;

import org.kuzmin.reversegeo.dto.GeoObjectDTO;
import org.kuzmin.reversegeo.json.GeoObjectCollection.GeoObjectHolder;
import org.kuzmin.reversegeo.json.GeocoderResponse;
import org.kuzmin.reversegeo.models.GeoObjectModel;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class JSONConverter {

    public List<GeoObjectModel> convert(String jsonString) {
        List<GeoObjectModel> result = new ArrayList<GeoObjectModel>();

        Gson gson = new GsonBuilder().create();
        GeocoderResponse response = gson.fromJson(jsonString, GeocoderResponse.class);

        GeoObjectDTO geoObjectDTO = new GeoObjectDTO();

        for (GeoObjectHolder geoObjectHolder : response.getResponse().getGeoObjectCollection().getFeatureMember()) {
            GeoObjectModel geoObjectModel = geoObjectDTO.convert(geoObjectHolder.getGeoObject());
            if (geoObjectModel != null) {
                result.add(geoObjectModel);
            }
        }

        return result;
    }
}
