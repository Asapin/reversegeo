package org.kuzmin.reversegeo.interfaces;

import java.util.List;

import org.kuzmin.reversegeo.models.Coordinates;
import org.kuzmin.reversegeo.models.GeoObjectModel;

public interface ReverseGeoService {

    List<GeoObjectModel> retrieveGeoObjects(Coordinates coordinates)
            throws Exception;

    String getUrl(Double longitude, Double latitude, String locale);
}
