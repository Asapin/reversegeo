package org.kuzmin.reversegeo.interfaces;

import java.util.Map;

public interface LocaleService {

    Map<String, String> getAvailableLocales();

    boolean checkLocaleCorrect(String locale);
}
