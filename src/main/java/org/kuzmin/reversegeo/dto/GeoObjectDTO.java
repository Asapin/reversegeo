package org.kuzmin.reversegeo.dto;

import org.kuzmin.reversegeo.json.AddressDetails;
import org.kuzmin.reversegeo.json.AdministrativeArea;
import org.kuzmin.reversegeo.json.Country;
import org.kuzmin.reversegeo.json.GeoObject;
import org.kuzmin.reversegeo.json.GeocoderMetaData;
import org.kuzmin.reversegeo.json.Locality;
import org.kuzmin.reversegeo.json.MetaDataProperty;
import org.kuzmin.reversegeo.json.Premise;
import org.kuzmin.reversegeo.json.SubAdministrativeArea;
import org.kuzmin.reversegeo.json.Thoroughfare;
import org.kuzmin.reversegeo.models.GeoObjectModel;

public class GeoObjectDTO {

    public GeoObjectModel convert(GeoObject geoObject) {
        MetaDataProperty metaDataProperty = geoObject.getMetaDataProperty();
        if (metaDataProperty == null) {
            return null;
        }

        GeocoderMetaData geocoderMetaData = metaDataProperty.getGeocoderMetaData();
        if (geocoderMetaData == null) {
            return null;
        }
        GeoObjectModel result = new GeoObjectModel();
        result.setKind(geocoderMetaData.getKind());

        AddressDetails addressDetails = geocoderMetaData.getAddressDetails();
        if (addressDetails == null) {
            return null;
        }

        Country country = addressDetails.getCountry();
        if (country == null) {
            return null;
        }
        result.setCountryName(country.getCountryName());

        AdministrativeArea administrativeArea = country.getAdministrativeArea();
        if (administrativeArea == null) {
            return result;
        }
        result.setAdministrativeAreaName(administrativeArea.getAdministrativeAreaName());

        SubAdministrativeArea subAdministrativeArea = administrativeArea.getSubAdministrativeArea();
        if (subAdministrativeArea == null) {
            return result;
        }
        result.setSubAdministrativeAreaName(subAdministrativeArea.getSubAdministrativeAreaName());

        Locality locality = subAdministrativeArea.getLocality();
        if (locality == null) {
            return result;
        }
        result.setLocalityName(locality.getLocalityName());

        Thoroughfare thoroughfare = locality.getThoroughfare();
        if (thoroughfare == null) {
            return result;
        }
        result.setThoroughfareName(thoroughfare.getThoroughfareName());

        Premise premise = thoroughfare.getPremise();
        if (premise == null) {
            return result;
        }
        result.setPremiseNumber(premise.getPremiseNumber());
        return result;
    }
}
